Spring Boot Custom Security
===========================

The application shows how to secure a spring boot application by 
----------------------------------------------------------------

	*	using a custom login form
	*	role based page view on login
	*	custom authentication success handler
	*	custom error display on invalid login
	*	in-memory authentication 

Requirements
------------

	* [Java Platform (JDK) 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
	* [Apache Maven 3.x](http://maven.apache.org/)

Quick Start
-----------

	1. `mvn spring-boot:run`
	2. Point your browser to [http://localhost:8080/](http://localhost:8080/)
	3. Login (username / password / role)
		*	user / user / USER
		*	admin / admin / ADMIN



Acknowledgments
---------------

	*	http://docs.spring.io/spring-security/site/docs/3.2.0.RELEASE/guides/form.html
	*	http://stackoverflow.com/questions/23661492/implement-logout-functionality-in-spring-boot