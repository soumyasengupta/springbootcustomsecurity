package soumya.example.springboot.customsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by soumya on 4/2/17.
 */
@SpringBootApplication
public class SpringBootCustomSecurityApplication {

    public static void main(String[] args){

        SpringApplication.run(SpringBootCustomSecurityApplication.class, args);

    }

}
