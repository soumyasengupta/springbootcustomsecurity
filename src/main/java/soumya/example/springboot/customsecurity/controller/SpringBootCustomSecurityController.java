package soumya.example.springboot.customsecurity.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by soumya on 4/2/17.
 */
@Controller
public class SpringBootCustomSecurityController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String displayLoginForm() {

        return "login";

    }

    @RequestMapping(value = "/login/{error}", method = RequestMethod.GET)
    public String displayLoginForm(Model model, @PathVariable final String error) {

        model.addAttribute("error", error);
        return "login";

    }

    @RequestMapping(value = "/page/user", method = RequestMethod.GET)
    public String displayuserPage() {

        return "userpage";

    }

    @RequestMapping(value = "/page/admin", method = RequestMethod.GET)
    public String displayAdminPage() {

        return "adminpage";

    }

}
